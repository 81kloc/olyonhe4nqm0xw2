<?php

namespace App\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvoiceItem
 *
 * @ORM\Table(name="invoice_item", indexes={@ORM\Index(name="invoice_item_invoice_id_fk", columns={"invoice_id"}), @ORM\Index(name="invoice_item_vat_rate_id_fk", columns={"vat_rate_id"})})
 * @ORM\Entity
 */
class InvoiceItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=190, nullable=true)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="net_price", type="float", precision=10, scale=0, nullable=false)
     */
    private $netPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="vat_price", type="float", precision=10, scale=0, nullable=false)
     */
    private $vatPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="gross_price", type="float", precision=10, scale=0, nullable=false)
     */
    private $grossPrice;

    /**
     * @var Invoice
     *
     * @ORM\ManyToOne(targetEntity="Invoice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     * })
     */
    private $invoice;

    /**
     * @var VatRate
     *
     * @ORM\ManyToOne(targetEntity="VatRate")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vat_rate_id", referencedColumnName="id")
     * })
     */
    private $vatRate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getNetPrice(): float
    {
        return $this->netPrice;
    }

    /**
     * @param float $netPrice
     */
    public function setNetPrice(float $netPrice): void
    {
        $this->netPrice = $netPrice;
    }

    /**
     * @return float
     */
    public function getVatPrice(): float
    {
        return $this->vatPrice;
    }

    /**
     * @param float $vatPrice
     */
    public function setVatPrice(float $vatPrice): void
    {
        $this->vatPrice = $vatPrice;
    }

    /**
     * @return float
     */
    public function getGrossPrice(): float
    {
        return $this->grossPrice;
    }

    /**
     * @param float $grossPrice
     */
    public function setGrossPrice(float $grossPrice): void
    {
        $this->grossPrice = $grossPrice;
    }

    /**
     * @return Invoice
     */
    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }

    /**
     * @param Invoice $invoice
     */
    public function setInvoice(Invoice $invoice): void
    {
        $this->invoice = $invoice;
    }

    /**
     * @return VatRate|null
     */
    public function getVatRate(): ?VatRate
    {
        return $this->vatRate;
    }

    /**
     * @param VatRate $vatRate
     */
    public function setVatRate(VatRate $vatRate): void
    {
        $this->vatRate = $vatRate;
    }

}

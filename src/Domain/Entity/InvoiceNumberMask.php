<?php

namespace App\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvoiceNumberMask
 *
 * @ORM\Table(name="invoice_number_mask", indexes={@ORM\Index(name="invoice_number_mask_user_id_fk", columns={"user_id"})})
 * @ORM\Entity
 */
class InvoiceNumberMask
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mask", type="string", length=20, nullable=false)
     */
    private $mask;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}

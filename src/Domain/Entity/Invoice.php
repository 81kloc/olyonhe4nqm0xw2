<?php
declare(strict_types=1);
namespace App\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Json;

/**
 * Invoice
 *
 * @ORM\Table(name="invoice", indexes={@ORM\Index(name="invoice_invoice_type_id_fk", columns={"invoice_type_id"}), @ORM\Index(name="invoice_contractor_id_fk", columns={"contractor_id"}), @ORM\Index(name="invoice_user_id_fk", columns={"user_id"})})
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\DoctrineInvoiceRepository")
 */
class Invoice
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_number", type="string", length=150, nullable=false)
     */
    private $invoiceNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descr", type="string", length=255, nullable=true)
     */
    private $descr;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contractor_json", type="json", nullable=false)
     */
    private $contractorJson;

    /**
     * @var float|null
     *
     * @ORM\Column(name="net_price", type="float", precision=10, scale=0, nullable=false)
     */
    private $netPrice;

    /**
     * @var float|null
     *
     * @ORM\Column(name="vat_price", type="float", precision=10, scale=0, nullable=false)
     */
    private $vatPrice;

    /**
     * @var float|null
     *
     * @ORM\Column(name="gross_price", type="float", precision=10, scale=0, nullable=false)
     */
    private $grossPrice;

    /**
     * @Assert\Type("\DateTimeInterface")
     *
     * @ORM\Column(name="created_at", type="date", nullable=false)
     */
    private $createdAt;

    /**
     * @var Contractor
     *
     * @ORM\ManyToOne(targetEntity="Contractor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contractor_id", referencedColumnName="id")
     * })
     */
    private $contractor;

    /**
     * @var InvoiceType|null
     *
     * @ORM\ManyToOne(targetEntity="InvoiceType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invoice_type_id", referencedColumnName="id")
     * })
     */
    private $invoiceType;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\InvoiceItem", mappedBy="invoice", orphanRemoval=true, cascade={"persist", "remove"})
     */
    public $invoiceItems;

    /**
     * @var string|null
     */
    private $contractorAutocomplete;

    /**
     * @var boolean|null
     *
     * @ORM\Column(name="vat_half", type="boolean")
     */
    private $vatHalf;

    /**
     * @var DictGtu
     *
     * @ORM\ManyToOne(targetEntity="DictGtu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dict_gtu_id", referencedColumnName="id")
     * })
     */
    private $dictGtu;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pay_day", type="integer", nullable=false)
     */
    private $payDay;

    public function __construct()
    {
        $this->invoiceItems = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getInvoiceNumber(): ?string
    {
        return $this->invoiceNumber;
    }

    /**
     * @param string $invoiceNumber
     */
    public function setInvoiceNumber(string $invoiceNumber): void
    {
        $this->invoiceNumber = $invoiceNumber;
    }

    /**
     * @return string|null
     */
    public function getDescr(): ?string
    {
        return $this->descr;
    }

    /**
     * @param string|null $descr
     */
    public function setDescr(?string $descr): void
    {
        $this->descr = $descr;
    }

    /**
     * @return float|null
     */
    public function getNetPrice(): ?float
    {
        return $this->netPrice;
    }

    /**
     * @param float $netPrice
     */
    public function setNetPrice(float $netPrice): void
    {
        $this->netPrice = $netPrice;
    }

    /**
     * @return float|null
     */
    public function getVatPrice(): ?float
    {
        return $this->vatPrice;
    }

    /**
     * @param float|null $vatPrice
     */
    public function setVatPrice(?float $vatPrice): void
    {
        $this->vatPrice = $vatPrice;
    }

    /**
     * @return float|null
     */
    public function getGrossPrice(): ?float
    {
        return $this->grossPrice;
    }

    /**
     * @param float|null $grossPrice
     */
    public function setGrossPrice(?float $grossPrice): void
    {
        $this->grossPrice = $grossPrice;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return InvoiceType|null
     */
    public function getInvoiceType(): ?InvoiceType
    {
        return $this->invoiceType;
    }

    /**
     * @param InvoiceType $invoiceType
     */
    public function setInvoiceType(InvoiceType $invoiceType): void
    {
        $this->invoiceType = $invoiceType;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function addInvoiceItem(InvoiceItem $invoiceItem): self
    {
        if (!$this->invoiceItems->contains($invoiceItem)) {
            $this->invoiceItems[] = $invoiceItem;
            $invoiceItem->setInvoice($this);
        }

        return $this;
    }

    public function removeInvoiceItem(InvoiceItem $invoiceItem): self
    {
        if ($this->invoiceItems->contains($invoiceItem)) {
            $this->invoiceItems->removeElement($invoiceItem);
            if ($invoiceItem->getInvoice() === $this) {
                $invoiceItem->setInvoice(null);
            }
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContractorAutocomplete(): ?string
    {
        return $this->contractorAutocomplete;
    }

    /**
     * @param string|null $contractorAutocomplete
     */
    public function setContractorAutocomplete(?string $contractorAutocomplete): void
    {
        $this->contractorAutocomplete = $contractorAutocomplete;
    }

    /**
     * @return string|null
     */
    public function getContractorJson(): ?string
    {
        return $this->contractorJson;
    }

    /**
     * @param string|null $contractorJson
     */
    public function setContractorJson(?string $contractorJson): void
    {
        $this->contractorJson = $contractorJson;
    }

    /**
     * @return Contractor|null
     */
    public function getContractor(): ?Contractor
    {
        return $this->contractor;
    }

    /**
     * @param Contractor $contractor
     */
    public function setContractor(Contractor $contractor): void
    {
        $this->contractor = $contractor;
    }

    public function __toString() {
        return $this->getInvoiceNumber();
    }

    /**
     * @return Collection
     */
    public function getInvoiceItems(): Collection
    {
        return $this->invoiceItems;
    }

    /**
     * @return bool|null
     */
    public function getVatHalf(): ?bool
    {
        return $this->vatHalf;
    }

    /**
     * @param bool|null $vatHalf
     */
    public function setVatHalf(?bool $vatHalf): void
    {
        $this->vatHalf = $vatHalf;
    }

    /**
     * @return DictGtu|null
     */
    public function getDictGtu(): ?DictGtu
    {
        return $this->dictGtu;
    }

    /**
     * @param DictGtu|null $dictGtu
     */
    public function setDictGtu(?DictGtu $dictGtu): void
    {
        $this->dictGtu = $dictGtu;
    }

    /**
     * @return int|null
     */
    public function getPayDay(): ?int
    {
        return $this->payDay;
    }

    /**
     * @param int|null $payDay
     */
    public function setPayDay(?int $payDay): void
    {
        $this->payDay = $payDay;
    }
}



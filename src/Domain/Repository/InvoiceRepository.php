<?php
declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Entity\Invoice;
use App\Domain\Entity\User;

interface InvoiceRepository
{
    public function create(Invoice $invoice) : void;

    public function listByUser(User $user): array;

    public function findById(User $user, int $id): ?Invoice;

    public function findByDate(User $user, string $year, ?string $month, ?int $invoiceType);
}
<?php
declare(strict_types=1);

namespace App\Domain\Repository;

interface InvoiceTypeRepository
{
    public function list(): array;
}
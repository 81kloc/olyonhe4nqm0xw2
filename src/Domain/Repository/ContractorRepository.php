<?php
declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Entity\Contractor;
use App\Domain\Entity\User;

interface ContractorRepository
{
    public function create(Contractor $contractor) : void;

    public function listByUser(User $user): array;

    public function findById(int $id): ?Contractor;

    public function search(User $user, string $search): array;
}
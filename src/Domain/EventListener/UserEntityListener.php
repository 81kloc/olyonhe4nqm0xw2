<?php
declare(strict_types=1);
namespace App\Domain\EventListener;

use App\Domain\Entity\Contractor;
use App\Domain\Entity\Invoice;
use App\Domain\Entity\InvoiceNumberMask;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserEntityListener
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        if ($entity instanceof Invoice || $entity instanceof InvoiceNumberMask) {
            $entity->setUser($this->tokenStorage->getToken()->getUser());
        }
    }
}
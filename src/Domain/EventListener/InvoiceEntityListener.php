<?php
declare(strict_types=1);

namespace App\Domain\EventListener;

use App\Domain\Entity\Invoice;
use App\Infrastructure\Services\InvoiceService;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class InvoiceEntityListener
{
    private $encoders;
    private $normalizers;
    /**
     * @var InvoiceService
     */
    private $invoiceService;

    public function __construct(InvoiceService $invoiceService)
    {
        $this->encoders = [new XmlEncoder(), new JsonEncoder()];
        $this->normalizers = [new ObjectNormalizer()];
        $this->invoiceService = $invoiceService;
    }

    public function prePersist(Invoice $invoice): void
    {
        $this->encoders = [new XmlEncoder(), new JsonEncoder()];

        $serializer = new Serializer($this->normalizers, $this->encoders);
        $contractor = clone $invoice->getContractor();
        $contractor->setUser(null);
        $jsonContent = $serializer->serialize($contractor, 'json');
        $invoice->setContractorJson($jsonContent);

        $this->invoiceService->reCalculatePrice($invoice);
    }

    public function preUpdate(Invoice $invoice, PreUpdateEventArgs $args): void
    {
        if($args->hasChangedField('contractor')){
            $serializer = new Serializer($this->normalizers, $this->encoders);
            $contractor = clone $invoice->getContractor();
            $contractor->setUser(null);
            $jsonContent = $serializer->serialize($contractor, 'json');
            $invoice->setContractorJson($jsonContent);
        }
    }
}
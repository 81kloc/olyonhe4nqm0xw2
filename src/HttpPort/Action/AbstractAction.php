<?php
declare(strict_types=1);

namespace App\HttpPort\Action;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

abstract class AbstractAction extends AbstractController
{
    /**
     * @var MessageBusInterface
     */
    private $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * Query for asked data.
     *
     * @param object $commandRequest
     *
     * @return mixed
     */
    public function ask($commandRequest)
    {
        return $this->messageBus->dispatch(
            $commandRequest
        )->last(HandledStamp::class);
    }
}
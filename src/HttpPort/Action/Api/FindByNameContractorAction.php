<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Api;

use App\Application\Command\Contractor\FindByNameContractorCommand;
use App\HttpPort\Action\AbstractAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

class FindByNameContractorAction extends AbstractAction
{
    public function __construct(MessageBusInterface $messageBus)
    {
        parent::__construct($messageBus);
    }

    public function __invoke(Request $request)
    {
        $items = $this->ask(new FindByNameContractorCommand($this->getUser(), $request->get('search')));

        return $this->json($items->getResult(), 200);
    }
}
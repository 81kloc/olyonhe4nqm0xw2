<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Api;


use App\HttpPort\Action\AbstractAction;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GetByNipContractorAction extends AbstractAction
{
    /**
     * @var HttpClientInterface
     */
    private $client;

    public function __construct(MessageBusInterface $messageBus, HttpClientInterface $client)
    {
        parent::__construct($messageBus);
        $this->client = $client;
    }

    public function __invoke(string $nip)
    {
        $response = $this->client->request(
            'GET',
            'https://wl-api.mf.gov.pl/api/search/nip/'.$nip.'?date='.(new \DateTime())->format("Y-m-d")
        );

//        $statusCode = $response->getStatusCode();
//        $content = $response->getContent();
        $content = $response->toArray();
//dump($content); die;
        $fullAddress = $content['result']['subject']['residenceAddress'] ?? $content['result']['subject']['workingAddress'];

        $parseAddress = explode(',', $fullAddress);
        $address = $parseAddress[0];
        $parseZipCodeWithCity = explode(' ', trim($parseAddress[1]));
        $zipCode = $parseZipCodeWithCity[0];
        unset($parseZipCodeWithCity[0]);
        $city = implode(' ', $parseZipCodeWithCity);

        $data = [
            'name' => $content['result']['subject']['name'] ?? '',
            'nip' =>  $content['result']['subject']['nip'] ?? $nip,
            'address' => $address,
            'zipCode' => $zipCode,
            'city' => $city,
        ];

        return $this->json($data, 200);

    }
}
<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Api;

use App\Application\Command\Invoice\GetByInvoiceByDateCommand;
use App\HttpPort\Action\AbstractAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

class GetByInvoiceByDateAction extends AbstractAction
{
    public function __construct(MessageBusInterface $messageBus)
    {
        parent::__construct($messageBus);
    }

    public function __invoke(Request $request)
    {
        $year = $request->get('year');
        $month = $request->get('month');
        $invoiceType = (int)$request->get('invoiceType', null);

        $items = $this->ask(new GetByInvoiceByDateCommand($this->getUser(), $year, $month, $invoiceType));

        return new JsonResponse([
                'html' => $this->renderView('panel/invoice/_list.html.twig', ['items' => $items->getResult()])
            ]
        );
    }
}
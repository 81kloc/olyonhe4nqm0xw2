<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Security;

use App\HttpPort\Action\AbstractAction;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginAction extends AbstractAction
{
    public function __construct(MessageBusInterface $messageBus)
    {
        parent::__construct($messageBus);
    }

    public function __invoke(AuthenticationUtils $authenticationUtils)
    {
         if ($this->getUser()) {
             return $this->redirectToRoute('dashboard');
         }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }
}
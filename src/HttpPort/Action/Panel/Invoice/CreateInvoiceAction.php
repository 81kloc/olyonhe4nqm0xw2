<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Panel\Invoice;

use App\Application\Command\Invoice\CreateInvoiceCommand;
use App\Domain\Entity\Invoice;
use App\Domain\Entity\InvoiceItem;
use App\HttpPort\Action\AbstractAction;
use App\Infrastructure\Form\InvoiceForm;
use App\Infrastructure\Validator\InvoiceValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

class CreateInvoiceAction extends AbstractAction
{
    /**
     * @var InvoiceValidator
     */
    private $validator;

    public function __construct(MessageBusInterface $messageBus, InvoiceValidator $validator)
    {
        parent::__construct($messageBus);
        $this->validator = $validator;
    }

    public function __invoke(Request $request)
    {
        $invoice = new Invoice();
        $invoice->setCreatedAt(new \DateTime('now'));
        $invoice->setPayDay(14);
        $invoiceItem = new InvoiceItem();
        $invoice->addInvoiceItem($invoiceItem);

        $form = $this->createForm(InvoiceForm::class, $invoice);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->ask(new CreateInvoiceCommand($invoice));

            return $this->redirectToRoute('dashboard');
        }

        return $this->render('panel/invoice/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
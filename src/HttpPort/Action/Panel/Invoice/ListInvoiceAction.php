<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Panel\Invoice;

use App\Application\Command\Invoice\GetByInvoiceByDateCommand;
use App\Application\Command\InvoiceType\InvoiceTypeListCommand;
use App\HttpPort\Action\AbstractAction;
use Symfony\Component\Messenger\MessageBusInterface;

class ListInvoiceAction extends AbstractAction
{
    public function __construct(MessageBusInterface $messageBus)
    {
        parent::__construct($messageBus);
    }

    public function __invoke()
    {
        $dateTime = (new \DateTime())->modify("-1 month");
        $invoiceType = $this->ask(new InvoiceTypeListCommand());
        $invoices = $this->ask(new GetByInvoiceByDateCommand($this->getUser(), $dateTime->format("Y"), $dateTime->format("m"), null));

        return $this->render('panel/invoice/list.html.twig', [
            'items' => $invoices->getResult(),
            'invoiceTypes' => $invoiceType->getResult(),
            'dateTime' => $dateTime
        ]);
    }
}
<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Panel\Invoice;

use App\Application\Command\Invoice\CreateInvoiceCommand;
use App\Application\Command\Invoice\EditInvoiceCommand;
use App\HttpPort\Action\AbstractAction;
use App\Infrastructure\Form\InvoiceForm;
use App\Infrastructure\Services\InvoiceService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

class EditInvoiceAction extends AbstractAction
{
    /**
     * @var InvoiceService
     */
    private $invoiceService;

    public function __construct(MessageBusInterface $messageBus, InvoiceService $invoiceService)
    {
        parent::__construct($messageBus);
        $this->invoiceService = $invoiceService;
    }

    public function __invoke(Request $request, int $id)
    {
        $invoice = $this->ask(new EditInvoiceCommand($this->getUser(), $id));

        if(!$invoice->getResult()){
            return $this->redirectToRoute('dashboard');
        }
        $data = $invoice->getResult();
        $contractorJson = json_decode($data->getContractorJson(), true);

        $data->setContractorAutocomplete($contractorJson['name']);

        $form = $this->createForm(InvoiceForm::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->invoiceService->reCalculatePrice($invoice->getResult());
            $this->ask(new CreateInvoiceCommand($invoice->getResult()));

            return $this->redirectToRoute('dashboard');
        }

        return $this->render('panel/invoice/create.html.twig', [
            'form' => $form->createView(),
            'contractorJson' => $contractorJson
        ]);
    }
}
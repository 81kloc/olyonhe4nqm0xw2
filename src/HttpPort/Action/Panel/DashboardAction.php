<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Panel;

use App\Application\Command\Invoice\ListInvoiceByUserCommand;
use App\HttpPort\Action\AbstractAction;
use Symfony\Component\Messenger\MessageBusInterface;

class DashboardAction extends AbstractAction
{
    public function __construct(MessageBusInterface $messageBus)
    {
        parent::__construct($messageBus);
    }

    public function __invoke()
    {
        $items = $this->ask(new ListInvoiceByUserCommand($this->getUser()));

        return $this->render('panel/invoice/list.html.twig', ['items' => $items]);
    }
}
<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Panel\Contractor;

use App\Application\Command\Contractor\CreateContractorCommand;
use App\Domain\Entity\Contractor;
use App\HttpPort\Action\AbstractAction;
use App\Infrastructure\Form\ContractorType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

class CreateContractorAction extends AbstractAction
{
    public function __construct(MessageBusInterface $messageBus)
    {
        parent::__construct($messageBus);
    }

    public function __invoke(Request $request)
    {
        $contractor = new Contractor();

        $form = $this->createForm(ContractorType::class, $contractor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contractor->setUser($this->getUser());
            $this->ask(new CreateContractorCommand($contractor));

            return $this->redirectToRoute('contractor_list');
        }

        return $this->render('panel/contractor/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
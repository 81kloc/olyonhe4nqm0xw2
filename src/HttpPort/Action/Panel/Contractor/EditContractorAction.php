<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Panel\Contractor;

use App\Application\Command\Contractor\CreateContractorCommand;
use App\Domain\Repository\ContractorRepository;
use App\HttpPort\Action\AbstractAction;
use App\Infrastructure\Form\ContractorType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

class EditContractorAction extends AbstractAction
{
    /**
     * @var ContractorRepository
     */
    private $contractorRepository;

    public function __construct(MessageBusInterface $messageBus, ContractorRepository $contractorRepository)
    {
        parent::__construct($messageBus);
        $this->contractorRepository = $contractorRepository;
    }

    public function __invoke(Request $request, int $id)
    {
        $contractor = $this->contractorRepository->findById($id);

        $form = $this->createForm(ContractorType::class, $contractor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->ask(new CreateContractorCommand($contractor));

            return $this->redirectToRoute('contractor_list');
        }

        return $this->render('panel/contractor/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
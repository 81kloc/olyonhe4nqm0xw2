<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Panel\Contractor;

use App\Application\Command\Contractor\ListContractorCommand;
use App\HttpPort\Action\AbstractAction;
use Symfony\Component\Messenger\MessageBusInterface;

class ListContractorAction extends AbstractAction
{
    public function __construct(MessageBusInterface $messageBus)
    {
        parent::__construct($messageBus);
    }

    public function __invoke()
    {
        $items = $this->ask(new ListContractorCommand($this->getUser()));

        return $this->render('panel/contractor/list.html.twig', ['items' => $items->getResult()]);
    }
}
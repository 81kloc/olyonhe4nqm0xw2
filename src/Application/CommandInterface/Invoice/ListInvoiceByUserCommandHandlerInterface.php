<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Invoice;

use App\Application\Command\Invoice\ListInvoiceByUserCommand;

interface ListInvoiceByUserCommandHandlerInterface
{
    public function __invoke(ListInvoiceByUserCommand $listInvoiceCommand);
}
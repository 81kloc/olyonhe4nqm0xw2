<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Invoice;

use App\Application\Command\Invoice\CreateInvoiceCommand;

interface CreateInvoiceCommandHandlerInterface
{
    public function __invoke(CreateInvoiceCommand $createInvoiceCommand);
}
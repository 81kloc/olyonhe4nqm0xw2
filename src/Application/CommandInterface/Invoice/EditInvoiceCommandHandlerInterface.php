<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Invoice;

use App\Application\Command\Invoice\EditInvoiceCommand;

interface EditInvoiceCommandHandlerInterface
{
    public function __invoke(EditInvoiceCommand $editInvoiceCommand);
}
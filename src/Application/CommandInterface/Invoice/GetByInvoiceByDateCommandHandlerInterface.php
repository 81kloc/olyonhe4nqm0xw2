<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Invoice;

use App\Application\Command\Invoice\GetByInvoiceByDateCommand;

interface GetByInvoiceByDateCommandHandlerInterface
{
    public function __invoke(GetByInvoiceByDateCommand $getByInvoiceByDateCommand);
}
<?php

namespace App\Application\CommandInterface\InvoiceType;

use App\Application\Command\InvoiceType\InvoiceTypeListCommand;

interface InvoiceTypeListCommandHandlerInterface
{
    public function __invoke(InvoiceTypeListCommand $invoiceTypeListCommand);
}
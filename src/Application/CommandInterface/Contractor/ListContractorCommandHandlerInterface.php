<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Contractor;

use App\Application\Command\Contractor\ListContractorCommand;

interface ListContractorCommandHandlerInterface
{
    public function __invoke(ListContractorCommand $listContractorCommand);
}
<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Contractor;

use App\Application\Command\Contractor\CreateContractorCommand;

interface CreateContractorCommandHandlerInterface
{
    public function __invoke(CreateContractorCommand $createContractorCommand);
}
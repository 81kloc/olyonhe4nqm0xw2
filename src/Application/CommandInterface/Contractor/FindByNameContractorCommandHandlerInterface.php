<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Contractor;

use App\Application\Command\Contractor\FindByNameContractorCommand;

interface FindByNameContractorCommandHandlerInterface
{
    public function __invoke(FindByNameContractorCommand $findByNameContractorCommand);
}
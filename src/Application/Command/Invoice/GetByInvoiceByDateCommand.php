<?php
declare(strict_types=1);

namespace App\Application\Command\Invoice;

use App\Domain\Entity\User;

class GetByInvoiceByDateCommand
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var string
     */
    private $year;
    /**
     * @var string|null
     */
    private $month;
    /**
     * @var string|null
     */
    private $invoiceType;

    public function __construct(User $user, string $year, ?string $month, ?int $invoiceType)
    {
        $this->user = $user;
        $this->year = $year;
        $this->month = $month;
        $this->invoiceType = $invoiceType;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getYear(): string
    {
        return $this->year;
    }

    /**
     * @return string|null
     */
    public function getMonth(): ?string
    {
        return $this->month;
    }

    /**
     * @return integer|null
     */
    public function getInvoiceType(): ?int
    {
        return $this->invoiceType;
    }
}
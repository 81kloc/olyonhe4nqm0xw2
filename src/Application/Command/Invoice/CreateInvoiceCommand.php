<?php
declare(strict_types=1);

namespace App\Application\Command\Invoice;

use App\Domain\Entity\Invoice;

class CreateInvoiceCommand
{
    /**
     * @var Invoice
     */
    private $invoice;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return Invoice
     */
    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }
}
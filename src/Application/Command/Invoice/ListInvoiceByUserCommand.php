<?php
declare(strict_types=1);

namespace App\Application\Command\Invoice;

use App\Domain\Entity\User;

class ListInvoiceByUserCommand
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
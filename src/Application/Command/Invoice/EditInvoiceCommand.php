<?php
declare(strict_types=1);

namespace App\Application\Command\Invoice;

use App\Domain\Entity\User;

class EditInvoiceCommand
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var int
     */
    private $id;

    public function __construct(User $user, int $id)
    {
        $this->user = $user;
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
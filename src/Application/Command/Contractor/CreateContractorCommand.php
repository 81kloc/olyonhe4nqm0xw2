<?php
declare(strict_types=1);

namespace App\Application\Command\Contractor;

use App\Domain\Entity\Contractor;

class CreateContractorCommand
{
    /**
     * @var Contractor
     */
    private $contractor;

    public function __construct(Contractor $contractor)
    {

        $this->contractor = $contractor;
    }

    /**
     * @return Contractor
     */
    public function getContractor(): Contractor
    {
        return $this->contractor;
    }
}
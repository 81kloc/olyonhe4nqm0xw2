<?php
declare(strict_types=1);

namespace App\Application\Command\Contractor;

use App\Domain\Entity\User;

class FindByNameContractorCommand
{
    /**
     * @var string
     */
    private $search;
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user, string $search)
    {
        $this->search = $search;
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getSearch(): string
    {
        return $this->search;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
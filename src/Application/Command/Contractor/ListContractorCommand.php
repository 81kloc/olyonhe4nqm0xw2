<?php
declare(strict_types=1);

namespace App\Application\Command\Contractor;

use App\Domain\Entity\User;

class ListContractorCommand
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
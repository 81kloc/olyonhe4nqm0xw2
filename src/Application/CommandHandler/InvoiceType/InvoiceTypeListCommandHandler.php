<?php
declare(strict_types=1);
namespace App\Application\CommandHandler\InvoiceType;

use App\Application\Command\InvoiceType\InvoiceTypeListCommand;
use App\Application\CommandInterface\InvoiceType\InvoiceTypeListCommandHandlerInterface;
use App\Domain\Repository\InvoiceTypeRepository;

class InvoiceTypeListCommandHandler implements InvoiceTypeListCommandHandlerInterface
{
    /**
     * @var InvoiceTypeRepository
     */
    private $repository;

    public function __construct(InvoiceTypeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param InvoiceTypeListCommand $invoiceTypeListCommand
     * @return array
     */
    public function __invoke(InvoiceTypeListCommand $invoiceTypeListCommand): array
    {
        return $this->repository->list();
    }

}
<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Contractor;

use App\Application\Command\Contractor\ListContractorCommand;
use App\Application\CommandInterface\Contractor\ListContractorCommandHandlerInterface;
use App\Domain\Repository\ContractorRepository;

class ListContractorCommandHandler implements ListContractorCommandHandlerInterface
{
    /**
     * @var ContractorRepository
     */
    private $contractorRepository;

    public function __construct(ContractorRepository $contractorRepository)
    {
        $this->contractorRepository = $contractorRepository;
    }

    public function __invoke(ListContractorCommand $listContractorCommand)
    {
        return $this->contractorRepository->listByUser($listContractorCommand->getUser());
    }

}
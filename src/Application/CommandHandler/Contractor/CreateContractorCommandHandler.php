<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Contractor;

use App\Application\Command\Contractor\CreateContractorCommand;
use App\Application\CommandInterface\Contractor\CreateContractorCommandHandlerInterface;
use App\Domain\Repository\ContractorRepository;

class CreateContractorCommandHandler implements CreateContractorCommandHandlerInterface
{

    /**
     * @var ContractorRepository
     */
    private $contractorRepository;

    public function __construct(ContractorRepository $contractorRepository)
    {

        $this->contractorRepository = $contractorRepository;
    }

    public function __invoke(CreateContractorCommand $createContractorCommand)
    {
        $this->contractorRepository->create($createContractorCommand->getContractor());
    }

}
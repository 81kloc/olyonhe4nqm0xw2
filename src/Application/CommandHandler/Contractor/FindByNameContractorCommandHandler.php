<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Contractor;

use App\Application\Command\Contractor\FindByNameContractorCommand;
use App\Application\CommandInterface\Contractor\FindByNameContractorCommandHandlerInterface;
use App\Domain\Repository\ContractorRepository;

class FindByNameContractorCommandHandler implements FindByNameContractorCommandHandlerInterface
{
    /**
     * @var ContractorRepository
     */
    private $contractorRepository;

    public function __construct(ContractorRepository $contractorRepository)
    {
        $this->contractorRepository = $contractorRepository;
    }

    public function __invoke(FindByNameContractorCommand $findByNameContractorCommand)
    {
        return $this->contractorRepository->search($findByNameContractorCommand->getUser(), $findByNameContractorCommand->getSearch());
    }

}
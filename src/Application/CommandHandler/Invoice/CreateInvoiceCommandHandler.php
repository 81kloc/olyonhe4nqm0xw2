<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Invoice;

use App\Application\Command\Invoice\CreateInvoiceCommand;
use App\Application\CommandInterface\Invoice\CreateInvoiceCommandHandlerInterface;
use App\Domain\Repository\InvoiceRepository;

class CreateInvoiceCommandHandler implements CreateInvoiceCommandHandlerInterface
{
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;

    public function __construct(InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

    public function __invoke(CreateInvoiceCommand $createInvoiceCommand)
    {
        $this->invoiceRepository->create($createInvoiceCommand->getInvoice());
    }

}
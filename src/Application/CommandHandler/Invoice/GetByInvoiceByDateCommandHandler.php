<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Invoice;

use App\Application\Command\Invoice\GetByInvoiceByDateCommand;
use App\Application\CommandInterface\Invoice\GetByInvoiceByDateCommandHandlerInterface;
use App\Domain\Repository\InvoiceRepository;

class GetByInvoiceByDateCommandHandler implements GetByInvoiceByDateCommandHandlerInterface
{
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;

    public function __construct(InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

    public function __invoke(GetByInvoiceByDateCommand $getByInvoiceByDateCommand)
    {
        return $this->invoiceRepository->findByDate($getByInvoiceByDateCommand->getUser(), $getByInvoiceByDateCommand->getYear(), $getByInvoiceByDateCommand->getMonth(), $getByInvoiceByDateCommand->getInvoiceType());
    }

}
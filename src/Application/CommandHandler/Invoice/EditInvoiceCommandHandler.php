<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Invoice;

use App\Application\Command\Invoice\EditInvoiceCommand;
use App\Application\CommandInterface\Invoice\EditInvoiceCommandHandlerInterface;
use App\Domain\Repository\InvoiceRepository;

class EditInvoiceCommandHandler implements EditInvoiceCommandHandlerInterface
{
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;

    public function __construct(InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

    public function __invoke(EditInvoiceCommand $editInvoiceCommand)
    {
        return $this->invoiceRepository->findById($editInvoiceCommand->getUser(), $editInvoiceCommand->getId());
    }

}
<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Invoice;

use App\Application\Command\Invoice\ListInvoiceByUserCommand;
use App\Application\CommandInterface\Invoice\ListInvoiceByUserCommandHandlerInterface;
use App\Domain\Repository\InvoiceRepository;

class ListInvoiceByUserByUserCommandHandler implements ListInvoiceByUserCommandHandlerInterface
{
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;

    public function __construct(InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

    public function __invoke(ListInvoiceByUserCommand $listInvoiceCommand)
    {
        return $this->invoiceRepository->listByUser($listInvoiceCommand->getUser());
    }

}
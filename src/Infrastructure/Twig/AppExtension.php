<?php
declare(strict_types=1);

namespace App\Infrastructure\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('jsonDecode', [$this, 'jsonDecode']),
        ];
    }

    public function jsonDecode($data)
    {
        return json_decode($data, true);
    }
}
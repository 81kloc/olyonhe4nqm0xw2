<?php
declare(strict_types=1);

namespace App\Infrastructure\Services;

use App\Domain\Entity\Invoice;

class InvoiceService
{
    public function reCalculatePrice(Invoice $invoice)
    {
        $netPrice = 0;
        $varPrice = 0;
        $grossPrice = 0;

        foreach ($invoice->getInvoiceItems() as $item){

            $netPrice= $netPrice + $item->getNetPrice();
            $varPrice= $varPrice + $item->getVatPrice();
            $grossPrice= $grossPrice + $item->getGrossPrice();
        }

        $invoice->setNetPrice($netPrice);
        $invoice->setVatPrice($varPrice);
        $invoice->setGrossPrice($grossPrice);

        return $invoice;
    }

    /**
     * @param Invoice $invoice
     * @return Invoice
     */
    public function reCalculateVatHalf(Invoice $invoice): Invoice
    {
        if($invoice->getVatHalf()){
            $netPrice = $invoice->getNetPrice();
            $vatPrice = round($invoice->getVatPrice()/2, 2);
            $netPrice = round(($netPrice+= $vatPrice)*0.75,2);

            $invoice->setNetPrice($netPrice);
            $invoice->setVatPrice($vatPrice);
        }

        return $invoice;
    }
}
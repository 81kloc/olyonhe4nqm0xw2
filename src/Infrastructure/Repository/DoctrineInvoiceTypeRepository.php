<?php
declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Repository\InvoiceTypeRepository;
use Doctrine\ORM\EntityRepository;

class DoctrineInvoiceTypeRepository extends EntityRepository implements InvoiceTypeRepository
{
    public function getAll()
    {
        return $this->createQueryBuilder('c')
            ->getQuery()
            ->setCacheable(true)
            ->getArrayResult()
            ;
    }

    public function list(): array
    {
        return $this->createQueryBuilder('c')
            ->getQuery()
            ->setCacheable(true)
            ->getArrayResult()
            ;
    }


}
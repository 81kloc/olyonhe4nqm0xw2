<?php
declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Contractor;
use App\Domain\Entity\User;
use App\Domain\Repository\ContractorRepository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Doctrine\Persistence\Mapping\ClassMetadata;

class DoctrineContractorRepository extends EntityRepository implements ContractorRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em, ClassMetadata $class)
    {
        parent::__construct($em, $class);
        $this->em = $em;
    }

    public function create(Contractor $contractor): void
    {
        $entity = $this->_em->merge($contractor);
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    public function listByUser(User $user): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getArrayResult()
            ;
    }

    public function flush()
    {
        $this->_em->flush();
    }

    public function findById(int $id): ?Contractor
    {
        return $this->find($id);
    }

    public function search(User $user, string $search): array
    {
        return $this->createQueryBuilder('c')
            ->setParameter('user', $user)
            ->andWhere('c.name like :search')
            ->orWhere('c.shortName like :search')
            ->andWhere('c.user = :user')
            ->setParameter('search', '%'.$search.'%')
            ->getQuery()
            ->getArrayResult()
            ;
    }

}
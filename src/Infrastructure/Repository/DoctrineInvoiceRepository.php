<?php
declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Invoice;
use App\Domain\Entity\User;
use App\Domain\Repository\InvoiceRepository;

use Doctrine\ORM\EntityRepository;

class DoctrineInvoiceRepository extends EntityRepository implements InvoiceRepository
{
    public function create(Invoice $invoice): void
    {
        $this->_em->persist($invoice);
        $this->_em->flush();
    }

    public function listByUser(User $user): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
            ;
    }

    public function flush()
    {
        $this->_em->flush();
    }

    public function findById(User $user, int $id): ?Invoice
    {
        return $this->findOneBy(['user' => $user, 'id' => $id]);
    }

    public function findByDate(User $user, string $year, ?string $month, ?int $invoiceType)
    {
        $query = $this->createQueryBuilder('c')
            ->andWhere('c.user = :user')
            ->setParameter('user', $user)
            ->andWhere('YEAR(c.createdAt) = :year')
            ->setParameter('year', $year);
            if($month){
                $query->andWhere('MONTH(c.createdAt) = :month')
                ->setParameter('month', str_pad($month, 2, '0', STR_PAD_LEFT));
            }
        if($invoiceType){
            $query->andWhere('c.invoiceType = :invoiceType')
                ->setParameter('invoiceType', $invoiceType);
        }

        return $query->getQuery()->getResult();
    }
}
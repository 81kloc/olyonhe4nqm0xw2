<?php
declare(strict_types=1);

namespace App\Infrastructure\Form;

use App\Domain\Entity\InvoiceItem;
use App\Domain\Entity\InvoiceType;
use App\Domain\Entity\VatRate;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InvoiceItemForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => 'off'
                ],
                'required' => false
            ])
            ->add('netPrice', TextType::class, [
                'attr' => [
                    'class' => 'form-control calculate netPrice',
                    'autocomplete' => 'off',
                    'pattern' => "[0-9]+(\.[0-9]{1,2})?%?"
                ]
            ])
            ->add('vatPrice', TextType::class, [
                'attr' => [
                    'class' => 'form-control calculate vatPrice',
                    'autocomplete' => 'off',
                    'pattern' => "[0-9]+(\.[0-9]{1,2})?%?"
                ]
            ])
            ->add('grossPrice', TextType::class, [
                'attr' => [
                    'class' => 'form-control calculate grossPrice',
                    'autocomplete' => 'off',
                    'pattern' => "[0-9]+(\.[0-9]{1,2})?%?"
                ]
            ])
            ->add('vatRate', EntityType::class, [
                'class' => VatRate::class,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'form-control vat-rate'
                ],
                'placeholder' => '---',
                'choice_attr' => function (VatRate $vatRate) {
                    return ['data-rate' => $vatRate->getRate() ];
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => InvoiceItem::class,
        ]);
    }

    public function getName()
    {
        return 'invoiceItems';
    }
    public function getBlockPrefix()
    {
        return 'invoiceItems';
    }
}
<?php
declare(strict_types=1);

namespace App\Infrastructure\Form;

use App\Domain\Entity\DictGtu;
use App\Domain\Entity\Invoice;
use App\Domain\Entity\InvoiceType;
use App\Infrastructure\Form\DataTransformer\ContractorAutocompleteTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InvoiceForm extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;

    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isEdit = $options['data']->getId() ? true : false;

        $builder
            ->add('invoiceType', EntityType::class, [
                'class' => InvoiceType::class,
                'required' => true,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => 'off'
                ],
                'placeholder' => '---'
            ])
            ->add('contractor', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => 'off',
                    'onkeypress' => 'return false;'
                ],
                'label' => 'Id',
            ])
            ->add('contractorAutocomplete', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => 'off'
                ],
            ])
            ->add('payDay', ChoiceType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => 'off'
                ],
                'choices'  => [
                    1 => 1,
                    5 => 5,
                    7 => 7,
                    14 => 14,
                    21 => 21,
                ],
                'empty_data' => 14,
                'label' => 'pay_day',
            ])
            ->add('createdAt', DateType::class, [
                'label' => 'invoice_created_at',
                'required' => true,
                'constraints' => [new Date(), new NotBlank()],
                'data' => new \DateTime('now'),
            ])
            ->add('dictGtu', EntityType::class, [
                'class' => DictGtu::class,
                'required' => false,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => 'off'
                ],
                'placeholder' => '---'
            ])
            ->add('invoiceNumber', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => 'off'
                ]
            ])
            ->add('descr', TextareaType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => 'off'
                ],
                'required' => false,
            ])
            ->add('vatHalf', CheckboxType::class, [
                'label'    => '50% VAT',
                'required' => false,
            ])
            //
            ->add('invoiceItems', CollectionType::class, [
                'entry_options' => ['label' => false],
                'entry_type'   => InvoiceItemForm::class,
                'allow_delete' => true,
                'allow_add' => true,
                'by_reference' => false,
            ])
            ->get('contractor')
            ->addModelTransformer(new ContractorAutocompleteTransformer($this->objectManager));
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Invoice::class,
        ]);
    }
}
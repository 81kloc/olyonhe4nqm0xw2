<?php
declare(strict_types=1);

namespace App\Infrastructure\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ContractorAutocompleteTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;

    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;

    }

    public function transform($value)
    {
        if (null === $value) {
            return null;
        }

        return $value->getId();
    }

    public function reverseTransform($value)
    {
        if (!$value) {
            return null;
        }

        $contractor = $this->objectManager->getRepository('App:Contractor')->find((int)$value);

        if (null === $contractor) {
            throw new TransformationFailedException(sprintf('There is no "%s" exists',
                $value
            ));
        }

        return $contractor;
    }

}
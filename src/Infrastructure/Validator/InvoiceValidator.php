<?php
declare(strict_types=1);

namespace App\Infrastructure\Validator;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class InvoiceValidator
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate(array $postData)
    {
        return $this->validator->validate($postData, $this->constraints($postData['invoice_form']));
    }

    protected function constraints(array $data = []): Collection
    {

        return new Collection([
            'invoiceType' => [new NotBlank()],
            'invoiceNumber' => [new NotBlank()],
            'contractor' => [new NotBlank()],
            'createdAt' => [new NotBlank()],
            'dictGtu' => [new NotBlank()],
            'descr' => [],
            'invoiceItems' => [new NotBlank()],
            '_token' => [new NotBlank()],

        ]);
    }
}